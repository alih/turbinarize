/*
 * turbinarize - a fast image binarizer that can double the resolution
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <jpeglib.h>
#include <jpegint.h>

#include "turbine.h"


void jpeg_idct_islow(j_decompress_ptr cinfo, jpeg_component_info * compptr,
          JCOEFPTR coef_block, JSAMPARRAY output_buf, JDIMENSION output_col);
void jpeg_idct_16x16(j_decompress_ptr cinfo, jpeg_component_info * compptr,
          JCOEFPTR coef_block, JSAMPARRAY output_buf, JDIMENSION output_col);

static void idct(j_decompress_ptr cinfo, jpeg_component_info * compptr,
          JCOEFPTR coef_block, JSAMPARRAY output_buf, JDIMENSION output_col)
{
    (void) cinfo;
    (void) compptr;
    jpeg_idct_islow(cinfo, compptr, coef_block, output_buf, output_col);
}

static void idct_x2(j_decompress_ptr cinfo, jpeg_component_info * compptr,
          JCOEFPTR coef_block, JSAMPARRAY output_buf, JDIMENSION output_col)
{
    (void) cinfo;
    (void) compptr;
    jpeg_idct_16x16(cinfo, compptr, coef_block, output_buf, output_col);
}

struct Turbine
{
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;
    FILE *stream;
    bool close_stream_on_free;
    jvirt_barray_ptr *coefs;
};

static Turbine *tb_new()
{
    Turbine *t = (Turbine *) malloc(sizeof(Turbine));
    memset(t, 0, sizeof(Turbine));
    t->cinfo.err = jpeg_std_error(&t->jerr);
    jpeg_create_decompress(&t->cinfo);
    return t;   
}

void tb_close(Turbine *t)
{
    jpeg_destroy_decompress(&t->cinfo);
    if (t->close_stream_on_free)
        fclose(t->stream);
    free(t);
}

static void read_coefs(Turbine *t)
{
    jpeg_read_header(&t->cinfo, /* require image: */ TRUE);
    t->cinfo.out_color_space = JCS_GRAYSCALE;
    jpeg_calc_output_dimensions(&t->cinfo);
    // jinit_color_deconverter(&t->cinfo); // get rid of chroma
    jinit_master_decompress(&t->cinfo);
    t->coefs = jpeg_read_coefficients(&t->cinfo);
}

static void tb_read_jpeg_from_memory(Turbine *t, unsigned char *data, int length)
{
    jpeg_mem_src(&t->cinfo, data, length);
    read_coefs(t);
}

static void tb_read_jpeg_from_stdio(Turbine *t, FILE *stream)
{
    t->stream = stream;
    jpeg_stdio_src(&t->cinfo, stream);
    read_coefs(t);
}

Turbine *tb_from_stdio(FILE *f)
{
    Turbine *t = tb_new();
    tb_read_jpeg_from_stdio(t, f);
    return t;
}

Turbine *tb_from_memory(unsigned char *data, int length)
{
    Turbine *t = tb_new();
    tb_read_jpeg_from_memory(t, data, length);
    return t;
}

int tb_width(Turbine *t)
{
    return t->cinfo.image_width;
}

int tb_height(Turbine *t)
{
    return t->cinfo.image_height;
}

int tb_sketch_width(Turbine *t)
{
    return t->cinfo.comp_info[0].width_in_blocks;
}

int tb_sketch_height(Turbine *t)
{
    return t->cinfo.comp_info[0].height_in_blocks;
}

static bool tb_read_jpeg(Turbine *t, const char *path)
{
    FILE *f = fopen(path, "rb");
    if (!f)
    {
        perror(path);
        return false;
    }
    tb_read_jpeg_from_stdio(t, f);
    t->close_stream_on_free = true;
    return true;
}

Turbine *tb_open(const char *path)
{
    Turbine *t = tb_new();
    bool ok = tb_read_jpeg(t, path);
    if (!ok)
    {
        tb_close(t);
        return NULL;
    }
    return t;
}


static JBLOCKROW tb_access_row(Turbine *t, int row, bool writable)
{
    j_decompress_ptr d = &t->cinfo;
    JBLOCKARRAY a = (d->mem->access_virt_barray) ((j_common_ptr) d, 
                                                  t->coefs[/* component id: */ 0],
                                                  /* start_row: */ row, 
                                                  /* num_rows: */ 1, 
                                                  /* writable: */ writable);
    return a[0];
}

static unsigned char clip(int n)
{
    if (n < 0)
        return 0;
    if (n > 255)
        return 255;
    return n;
}

void tb_get_sketch(Turbine *t, unsigned char *out, int stride)
{
    int w = tb_sketch_width(t);
    int h = tb_sketch_height(t);
    for (int i = 0; i < h; i++)
    {
        JBLOCKROW row = tb_access_row(t, i, false);
        for (int j = 0; j < w; j++)
        {
            JBLOCK *block = row + j;
            out[i * stride + j] = clip((**block >> 3) + 128);
        }
    }
}

void tb_get_grayscale(Turbine *t, unsigned char *out, int stride)
{
    int out_width = tb_width(t);   
    int out_height = tb_height(t);   
    
    int w = tb_sketch_width(t);
    int h = tb_sketch_height(t);

    unsigned char tmp[DCTSIZE2];

    j_decompress_ptr cinfo = &t->cinfo;
    jpeg_component_info *compptr = &cinfo->comp_info[0];

    for (int y = 0; y < h; y++)
    {
        unsigned char *out_rows[DCTSIZE];
        bool y_crop = y * DCTSIZE + 7 >= out_height;

        if (y_crop)
        {
            for (int i = 0; i < DCTSIZE; i++)
                out_rows[i] = &tmp[i * DCTSIZE];
        }
        else
        {
            for (int i = 0; i < DCTSIZE; i++)
                out_rows[i] = out + (y * DCTSIZE + i) * stride;
        }

        JBLOCKROW block_row = tb_access_row(t, y, false);
        for (int x = 0; x < w; x++)
        {
            JCOEFPTR block = block_row[x];
            bool x_crop = x * DCTSIZE + 7 >= out_width;
            if (!y_crop && x_crop)
            {
                for (int i = 0; i < DCTSIZE; i++)
                    out_rows[i] = &tmp[i * DCTSIZE];
            }
            bool crop = x_crop || y_crop;
            int col = crop  ?  0  :  x * DCTSIZE;
            idct(cinfo, compptr, block, out_rows, col);
            
            if (crop)
            {
                int x0 = x * DCTSIZE;
                int y0 = y * DCTSIZE;
                int i_cap = y_crop  ?  out_height  :  y0 + DCTSIZE;
                int j_cap = x_crop  ?  out_width   :  x0 + DCTSIZE;
                for (int i = y0; i < i_cap; i++) for (int j = x0; j < j_cap; j++)
                    out[i * stride + j] = tmp[(i - y0) * DCTSIZE + j - x0];
            }
        }
    } // for
} // tb_get_grayscale()

// ______________________________   implant   ________________________________

static void implant_1x1(Turbine *t, unsigned char threshold)
{
    JBLOCKROW row = tb_access_row(t, 0, true);
    **row += 1024 - (threshold << 3);
}

static void dct3(int a, int b, int c, int *result)
{
    result[0] = a + 6 * b + c;
    result[1] = (a - c) << 1;
    result[2] = a - 2 * b + c;
}

static void implant_in_block_horizontal(JCOEFPTR block, const unsigned char pixels[3])
{
    int result[3];
    dct3(pixels[0], pixels[1], pixels[2], result);
    block[0] += 1024 - result[0];
    block[1] -= (result[1] * 181) >> 8;
    block[2] -= (result[2] * 181) >> 8;
}

static void implant_in_block_vertical(JCOEFPTR block, const unsigned char pixels[3])
{
    int result[3];
    dct3(pixels[0], pixels[1], pixels[2], result);
    block[0] += 1024 - result[0];
    block[8] -= (result[1] * 181) >> 8;
    block[16] -= (result[2] * 181) >> 8;
}
        
        
static void implant_in_block(JCOEFPTR block, 
                             const unsigned char above[3], 
                             const unsigned char current[3], 
                             const unsigned char below[3])
{
    int buf[9];
    int result[9];
    dct3(above[0], current[0], below[0], buf); 
    dct3(above[1], current[1], below[1], buf + 3); 
    dct3(above[2], current[2], below[2], buf + 6); 

    dct3(buf[0], buf[3], buf[6], result); 
    dct3(buf[1], buf[4], buf[7], result + 3); 
    dct3(buf[2], buf[5], buf[8], result + 6); 

    // 1 / 4 * sqrt(2) ~= 181/1024
    result[1] *= 181;
    result[2] *= 181;
    result[3] *= 181;
    result[6] *= 181;

    block[0] += 1024 - (result[0] >> 3);
    // block[0] -= result[0] >> 3;
    block[1] -= result[1] >> 11;
    block[2] -= result[2] >> 11;
    block[8] -= result[3] >> 11;
    block[9] -= result[4] >> 4;
    block[10] -= result[5] >> 4;
    block[16] -= result[6] >> 11;
    block[17] -= result[7] >> 4;
    block[18] -= result[8] >> 4;
}

static void setup_fake_row_leftmost(unsigned char *fake_row, 
                                    const unsigned char *real_row)
{
    fake_row[0] = fake_row[1] = real_row[0];
    fake_row[2] = real_row[1];
}

static void setup_fake_row_leftmost_with_stride(unsigned char *fake_row, 
                                                const unsigned char *real_row,
                                                int stride)
{
    fake_row[0] = fake_row[1] = real_row[0];
    fake_row[2] = real_row[stride];
}

static void setup_fake_row_rightmost(unsigned char *fake_row, 
                                    const unsigned char *real_row,
                                    int n)
{
    fake_row[0] = real_row[n - 2];
    fake_row[1] = fake_row[2] = real_row[n - 1];
}

static void setup_fake_row_rightmost_with_stride(unsigned char *fake_row, 
                                                 const unsigned char *real_row,
                                                 int stride,
                                                 int n)
{
    fake_row[0] = real_row[(n - 2) * stride];
    fake_row[1] = fake_row[2] = real_row[(n - 1) * stride];
}

static void implant_in_row(JBLOCKROW row,
                           const unsigned char *above,
                           const unsigned char *current,
                           const unsigned char *below,
                           int n)
{
    assert(n > 1);

    unsigned char fake_above[3];
    unsigned char fake_current[3];
    unsigned char fake_below[3];

    setup_fake_row_leftmost(fake_above, above);
    setup_fake_row_leftmost(fake_current, current);
    setup_fake_row_leftmost(fake_below, below);
    implant_in_block(row[0], fake_above, fake_current, fake_below);

    for (int i = 0, i_cap = n - 2; i < i_cap; i++)
        implant_in_block(row[i + 1], above + i, current + i, below + i);
    
    setup_fake_row_rightmost(fake_above, above, n);
    setup_fake_row_rightmost(fake_current, current, n);
    setup_fake_row_rightmost(fake_below, below, n);
    implant_in_block(row[n - 1], fake_above, fake_current, fake_below);
}

static void implant_horizontal(Turbine *t, const unsigned char *threshold, int n)
{
    JBLOCKROW row = tb_access_row(t, 0, true);
    unsigned char fake_row[3];
    
    setup_fake_row_leftmost(fake_row, threshold);
    implant_in_block_horizontal(row[0], fake_row);

    for (int i = 0, i_cap = n - 2; i < i_cap; i++)
        implant_in_block_horizontal(row[i + 1], threshold + i);

    setup_fake_row_rightmost(fake_row, threshold, n);
    implant_in_block_horizontal(row[n - 1], fake_row);
}

static void implant_vertical(Turbine *t, const unsigned char *threshold, int stride, int n)
{
    unsigned char fake_row[3];

    setup_fake_row_leftmost_with_stride(fake_row, threshold, stride);
    JBLOCKROW row = tb_access_row(t, 0, true);
    implant_in_block_vertical(*row, fake_row);

    for (int i = 0, i_cap = n - 2; i < i_cap; i++)
    {
        row = tb_access_row(t, i + 1, true);
        implant_in_block_vertical(*row, threshold + i);
    }

    setup_fake_row_rightmost_with_stride(fake_row, threshold, stride, n);
    row = tb_access_row(t, n - 1, true);
    implant_in_block_vertical(*row, fake_row);
}

void tb_implant(Turbine *t, const unsigned char *threshold, int stride)
{
    int w = tb_sketch_width(t);
    int h = tb_sketch_height(t);
    
    if (w <= 0 || h <= 0)
        return;

    if (w == 1 && h == 1)
    {
        implant_1x1(t, *threshold);
        return;
    }

    if (h == 1)
    {
        implant_horizontal(t, threshold, w);
        return;
    }

    if (w == 1)
    {
        implant_vertical(t, threshold, stride, h);
        return;
    }

    // generic case: w, h > 1
    JBLOCKROW row = tb_access_row(t, 0, true);
    implant_in_row(row, threshold, threshold, threshold + stride, w);
    for (int y = 1; y < h - 1; y++)
    {
        row = tb_access_row(t, y, true);
        implant_in_row(row, 
                       threshold + (y - 1) * stride,
                       threshold + y * stride,
                       threshold + (y + 1) * stride, 
                       w);
    }
    row = tb_access_row(t, h - 1, true);
    implant_in_row(row, 
                   threshold + (h - 2) * stride,
                   threshold + (h - 1) * stride,
                   threshold + (h - 1) * stride, 
                   w);
}

// ________________   thresholding   ___________________

void tb_threshold(Turbine *t, unsigned char *result, int stride)
{
    int out_height = tb_height(t);   
    
    int w = tb_sketch_width(t);
    int h = tb_sketch_height(t);

    unsigned char tmp[DCTSIZE2];
    unsigned char *tmp_rows[DCTSIZE];

    for (int i = 0; i < DCTSIZE; i++)
        tmp_rows[i] = tmp + i * DCTSIZE;

    j_decompress_ptr cinfo = &t->cinfo;
    jpeg_component_info *compptr = &cinfo->comp_info[0];

    for (int y = 0; y < h; y++)
    {
        JBLOCKROW block_row = tb_access_row(t, y, false);

        for (int x = 0; x < w; x++)
        {
            JCOEFPTR block = block_row[x];
            idct(cinfo, compptr, block, tmp_rows, 0);

            for (int i = 0; i < DCTSIZE; i++)
            {
                int sum = 0;
                for (int j = 0; j < DCTSIZE; j++)
                    sum |= (tmp[i * DCTSIZE + j] & 0x80) >> j;
                int yy = y * DCTSIZE + i;
                if (yy < out_height)
                    result[yy * stride + x] = ~sum;
            }
        }
    } // for
}

void tb_threshold_x2(Turbine *t, unsigned char *result, int stride)
{
    int out_height = 2 * tb_height(t);   
    
    int w = tb_sketch_width(t);
    int h = tb_sketch_height(t);

    unsigned char tmp[DCTSIZE2 * 4];
    unsigned char *tmp_rows[DCTSIZE * 2];

    for (int i = 0; i < DCTSIZE * 2; i++)
        tmp_rows[i] = tmp + i * DCTSIZE * 2;

    j_decompress_ptr cinfo = &t->cinfo;
    jpeg_component_info *compptr = &cinfo->comp_info[0];

    for (int y = 0; y < h; y++)
    {
        JBLOCKROW block_row = tb_access_row(t, y, false);

        for (int x = 0; x < w; x++)
        {
            JCOEFPTR block = block_row[x];
            idct_x2(cinfo, compptr, block, tmp_rows, 0);

            for (int i = 0; i < DCTSIZE * 2; i++)
            {
                int sum1 = 0;
                for (int j = 0; j < DCTSIZE; j++)
                    sum1 |= (tmp[i * DCTSIZE * 2 + j] & 0x80) >> j;
                int sum2 = 0;
                for (int j = 0; j < DCTSIZE; j++)
                    sum2 |= (tmp[i * DCTSIZE * 2 + DCTSIZE + j] & 0x80) >> j;
                int yy = y * DCTSIZE * 2 + i;
                if (yy < out_height)
                {
                    result[yy * stride + 2 * x] = ~sum1;
                    result[yy * stride + 2 * x + 1] = ~sum2;
                }
            }
        }
    } // for
}
