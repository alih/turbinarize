/*
 * turbinarize - a fast image binarizer that can double the resolution
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include <g4.h>

#include "sauvola.h"
#include "turbine.h"

#define DEFAULT_W 3
#define DEFAULT_K 0.3f

const char *input_path = NULL;
const char *output_path = NULL;
int   sauvola_w = DEFAULT_W;
float sauvola_k = DEFAULT_K;
bool x2 = false;

static void process()
{
    Turbine *t = tb_open(input_path);

    int sw = tb_sketch_width(t);
    int sh = tb_sketch_height(t);
    unsigned char *sketch = (unsigned char *) malloc(sw * sh);
    tb_get_sketch(t, sketch, sw);

    unsigned char *th = (unsigned char *) malloc(sw * sh);
    sauvola(sketch, sw, sh, sw, th, sw,
            sauvola_w, sauvola_k);
  
    tb_implant(t, th, sw);

    int w = tb_width(t);
    int h = tb_height(t);

    if (x2)
    {
        unsigned char *bin = (unsigned char *) malloc(4 * sw * h);
        tb_threshold_x2(t, bin, sw * 2);

        g4_save(output_path, bin, w * 2, h * 2, sw * 2);

        free(bin);
    }
    else
    {
        unsigned char *bin = (unsigned char *) malloc(sw * h);
        tb_threshold(t, bin, sw);

        g4_save(output_path, bin, w, h, sw);

        free(bin);
    }
    free(th);
    free(sketch);

    tb_close(t);
}

static void print_usage(const char *prog_name)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "    %s [options] input.jpg output.tif\n", prog_name);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "    -w <number>       Set half window size for the 8x smaller image\n");
    fprintf(stderr, "                           (default: %d)\n\n", DEFAULT_W);
    fprintf(stderr, "    -k <number>       Set Sauvola's k (the smaller, the darker the result):\n");
    fprintf(stderr, "                           (default: %.2f)\n\n", (float) DEFAULT_K);
    fprintf(stderr, "    -2 or -x2         Double the output size\n");
}

static bool accept_int_option(const char *arg, const char *next,
                              const char *option, 
                              int min, int max, int *value, int *index)
{
    if (strcmp(arg, option))
        return false;

    if (!next)
    {
        fprintf(stderr, "A value expected after %s", option);
        exit(EXIT_FAILURE);
    }

    char *endptr;
    long v = strtol(next, &endptr, 0);
    if (*endptr)
    {
        fprintf(stderr, "Error parsing number for %s option: %s\n", option, next);
        exit(EXIT_FAILURE);
    }

    if (v < min || v > max)
    {
        fprintf(stderr,
                "The value for %s option should be between %d and %d, but found %ld\n", 
                option, min, max, v);
        exit(EXIT_FAILURE);
    }

    *value = (int) v;
    (*index)++;
    return true;
}

static bool accept_float_option(const char *arg, const char *next,
                                const char *option, 
                                float min, float max, float *value, int *index)
{
    if (strcmp(arg, option))
        return false;

    if (!next)
    {
        fprintf(stderr, "A value expected after %s\n", option);
        exit(EXIT_FAILURE);
    }

    char *endptr;
    double v = strtod(next, &endptr);
    if (*endptr)
    {
        fprintf(stderr, "Error parsing number for %s option: %s\n", option, next);
        exit(EXIT_FAILURE);
    }

    if (v < min || v > max)
    {
        fprintf(stderr,
                "The value for %s option should be between %f and %f, but found %lf\n", 
                option, min, max, v);
        exit(EXIT_FAILURE);
    }

    *value = (float) v;
    (*index)++;
    return true;
}

static bool accept_bool_option(const char *arg, const char *option, bool *value)
{
    if (strcmp(arg, option))
        return false;
    *value = true;
    return true;
}

int main(int argc, const char *const *argv)
{
    bool accept_options = true;
    for (int i = 1; i < argc; i++)
    {
        const char *arg = argv[i];
        const char *next = i == argc - 1 ? NULL 
                                         : argv[i + 1];
        if (accept_options && *arg == '-')
        {
            if (!strcmp(arg, "--"))
            {
                accept_options = false;
            }
            else if (!accept_int_option(arg, next, "-w", 0, INT_MAX, &sauvola_w, &i))
                 if (!accept_float_option(arg, next, "-k", 0, 1, &sauvola_k, &i))
                 if (!accept_bool_option(arg, "-2", &x2))
                 if (!accept_bool_option(arg, "-x2", &x2))
                 {
                     fprintf(stderr, "Unknown option: %s\n", arg);
                     print_usage(argv[0]);
                     exit(EXIT_FAILURE);
                 }
        }
        else if (!input_path)
            input_path = arg;
        else if (!output_path)
            output_path = arg;
        else
        {
            print_usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    } // for arg in argv

    if (!input_path || !output_path)
    {
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    process();

    return EXIT_SUCCESS;
}
