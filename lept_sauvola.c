#include <allheaders.h>

int main(int argc, const char *const *argv)
{
    PIX *image = pixReadJpeg(argv[1], 
                             /* need colormap: */ 0,
                             /* reduction: */ 1, 
                             /* number of warnings: */ NULL,
                             /* hint: */ L_JPEG_READ_LUMINANCE);
    
    int sauvola_w = 24;
    float sauvola_k = 0.3;

    int add_border = 1;
    
    PIX *binarized = NULL;
    pixSauvolaBinarize(image, sauvola_w, sauvola_k, add_border,
                       NULL, NULL, NULL, &binarized);

    pixWriteTiff(argv[2], binarized, IFF_TIFF_G4, "w");

    pixDestroy(&image);
}
