/*
 * turbinarize - a fast image binarizer that can double the resolution
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAUVOLA_H
#define SAUVOLA_H

#define SAUVOLA_DEFAULT_HW 20
#define SAUVOLA_DEFAULT_K 0.3f

void sauvola(const unsigned char *src, 
             int width, 
             int height,
             int src_stride,
             unsigned char *thresholds,
             int threshold_stride,
             int half_window,
             float k);

#endif
