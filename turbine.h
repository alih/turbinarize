/*
 * turbinarize - a fast image binarizer that can double the resolution
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TURBINE_H
#define TURBINE_H

#include <stdbool.h>
#include <stdio.h>

typedef struct Turbine Turbine;

/**
 * Open a JPEG file by path.
 */
Turbine *tb_open(const char *path);

/**
 * Open a JPEG file using stdio's FILE *.
 */
Turbine *tb_from_stdio(FILE *f);

/**
 * Open a JPEG image in memory.
 */
Turbine *tb_from_memory(unsigned char *data, int length);

void tb_close(Turbine *);

int tb_width(Turbine *);
int tb_height(Turbine *);

/**
 * Get the width of the 8x downsampled image ("sketch").
 */
int tb_sketch_width(Turbine *);

/**
 * Get the height of the 8x downsampled image ("sketch").
 */
int tb_sketch_height(Turbine *);

void tb_get_sketch(Turbine *, unsigned char *result, int stride);

/**
 * Uncompress the JPEG image normally (except color).
 * The memory should be pre-allocated to contain tb_height() rows 
 * of at least tb_width() pixels each.
 */
void tb_get_grayscale(Turbine *, unsigned char *result, int stride);

void tb_implant(Turbine *, const unsigned char *sketch_threshold, int stride);

/**
 * Get the binarized image.
 * The image will be tb_width() bits wide,
 * which is the same as tb_sketch_width() bytes wide.
 */
void tb_threshold(Turbine *, unsigned char *result, int stride);

/**
 * Get the binarized image with double resoultion.
 * The memory rows needs to be 2 * tb_sketch_width() bytes wide
 * even if 2 * tb_width() bits require one less byte.
 */
void tb_threshold_x2(Turbine *t, unsigned char *result, int stride);

#endif
