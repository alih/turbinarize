/*
 * turbinarize - a fast image binarizer that can double the resolution
 * Copyright (C) 2015-2016  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// I looked into Leptonica and Ocropus while writing this.
// Leptonica is licensed under 2-clause BSD, 
// Ocropus is licensed under Apache 2.

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include "sauvola.h"

typedef uint32_t IntegralImage;

static IntegralImage *integrate(const unsigned char *pixels, 
                                int width, 
                                int height, 
                                int stride)
{
    int s = width + 1;
    IntegralImage *ii = (IntegralImage *) malloc(s * (height + 1) * sizeof(IntegralImage));
    for (int j = 0; j <= width; j++)
        ii[j] = 0;
    for (int i = 1; i <= height; i++)
    {
        IntegralImage *prev = ii + s * (i - 1);
        IntegralImage *row = ii + s * i;
        const unsigned char *pixel_row = pixels + (i - 1) * stride;
        row[0] = 0;
        for (int j = 1; j <= width; j++)
            row[j] = row[j - 1] + prev[j] - prev[j - 1] + pixel_row[j - 1];
    }
    return ii; 
}

static IntegralImage *integrate_sq(const unsigned char *pixels, 
                                   int width, 
                                   int height, 
                                   int stride)
{
    int s = width + 1;
    IntegralImage *ii = (IntegralImage *) malloc(s * (height + 1) * sizeof(IntegralImage));
    for (int j = 0; j <= width; j++)
        ii[j] = 0;
    for (int i = 1; i <= height; i++)
    {
        IntegralImage *prev = ii + s * (i - 1);
        IntegralImage *row = ii + s * i;
        const unsigned char *pixel_row = pixels + (i - 1) * stride;
        row[0] = 0;
        for (int j = 1; j <= width; j++)
        {
            IntegralImage sq = pixel_row[j - 1] * pixel_row[j - 1];
            row[j] = row[j - 1] + prev[j] - prev[j - 1] + sq;
        }
    }
    return ii; 
}

static IntegralImage integral_image_get(IntegralImage *ii, int w,
                          int x0, int y0, int x1, int y1)
{
    int stride = w + 1;
    return ii[y1 * stride + x1] + ii[y0 * stride + x0] \
         - ii[y1 * stride + x0] - ii[y0 * stride + x1];
}
 
static inline int min(int a, int b)
{
    return a < b ? a : b;
}

static inline int max(int a, int b)
{
    return a > b ? a : b;
}

static inline int clip(int n, int a, int b)
{
    if (n < a)
        return a;
    if (n > b)
        return b;
    return n;
}

void sauvola(const unsigned char *src, 
             int width, 
             int height,
             int src_stride,
             unsigned char *thresholds,
             int thresholds_stride,
             int half_window,
             float k)
{
    if (width <= 0 || height <= 0)
        return;

    if (width == 1 && height == 1)
    {
        // avoiding area == 1 case because of / (area - 1)
        *thresholds = *src;
        return;
    }

    IntegralImage *ii  = integrate(src, width, height, src_stride);
    IntegralImage *ii2 = integrate_sq(src, width, height, src_stride);

    for (int i = 0; i < height; i++) for (int j = 0; j < width; j++)
    {
        int xmin = max(0, j - half_window);
        int ymin = max(0, i - half_window);
        int xmax = min(width - 1, j + half_window);
        int ymax = min(height - 1, i + half_window);
        int area = (xmax - xmin + 1) * (ymax - ymin + 1);
        // area can't be 0 here
        // proof (assuming half_window >= 0): 
        // we'll prove that (xmax-xmin+1) > 0,
        // (ymax-ymin+1) is analogous
        // It's the same as to prove: xmax >= xmin
        // width - 1 >= 0               since width > i >= 0
        // i + half_window >= 0         since i >= 0, whalf >= 0
        // i + whalf >= i - half_window since half_window >= 0
        // width - 1 >= i - half_window since width > i
        // --IM
        assert(xmax - xmin + 1 > 0);
        assert(ymax - ymin + 1 > 0);
        assert(area);

        uint32_t diff   = integral_image_get(ii,  width, xmin, ymin, xmax + 1, ymax + 1);
        uint32_t sqdiff = integral_image_get(ii2, width, xmin, ymin, xmax + 1, ymax + 1);

        double mean = (double) diff / area;
        double std = sqrt((sqdiff - (double) diff * diff / area) / (area - 1));
        double threshold = mean * (1 + k * ((std / 128) - 1));
        
        thresholds[i * thresholds_stride + j] = 
            (unsigned char) clip((int) threshold, 0, 255);
    }

    free(ii);
    free(ii2);
}
