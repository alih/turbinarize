# Turbinarize
Turbinarize is a fast image binarizer written specifically for JPEG images. Currently it only does approximate Sauvola but other algorithms can be added.  
Also, turbinarize can double the resolution.

## How fast is it?
The whole job including loading JPEG and writing TIFF is roughly 3x faster on my machine than a Leptonica-based implementation doing Sauvola.

## Why is it fast?
It interleaves JPEG decompression and binarization, subtracting the threshold image from the image in DCT-coefficient space. Since the threshold image is smooth, only the low-frequency coefficients need to be touched.

## Building
Install ninja (Debian & Ubuntu: sudo apt-get build-ninja).
Run build.sh.
